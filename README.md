# OpenML dataset: Parkinson_Dataset

https://www.openml.org/d/46094

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Description:
This dataset, named "parkinsons data.csv", encompasses a collection of voice measurement data from individuals, some of whom have Parkinson's disease. It includes a diverse range of voice signal attributes aimed at assisting in the early diagnosis and tracking of Parkinson's disease through non-invasive methods. The dataset contains several columns such as 'MDVP:Fo(Hz)', 'MDVP:Fhi(Hz)', 'MDVP:Flo(Hz)', signifying the voice frequency measurements, and others like 'MDVP:Jitter(%)', 'MDVP:Shimmer', 'NHR', 'HNR', relaying the variation in voice frequency and amplitude. 'Status' is a binary indicator where '1' denotes the presence and '0' the absence of Parkinson's disease. Additional metrics relevant to voice disorders are included, covering various aspects of voice quality and dynamics, such as 'RPDE', 'DFA', 'spread1', 'spread2', 'D2', and 'PPE', offering a comprehensive set of features for analysis.

Attribute Description:
1. MDVP:Fo(Hz): Average vocal fundamental frequency.
2. MDVP:Fhi(Hz): Maximum vocal fundamental frequency.
3. MDVP:Flo(Hz): Minimum vocal fundamental frequency.
4. MDVP:Jitter(%), MDVP:Jitter(Abs), MDVP:RAP, MDVP:PPQ, Jitter:DDP: Various measures of variation in frequency.
5. MDVP:Shimmer, MDVP:Shimmer(dB), Shimmer:APQ3, Shimmer:APQ5, MDVP:APQ, Shimmer:DDA: Different measures of variation in amplitude.
6. NHR, HNR: Ratios depicting noise components in the voice.
7. Status: Binary status for the presence of Parkinson's disease.
8. RPDE, DFA, spread1, spread2, D2, PPE: Nonlinear dynamical measurements.

Use Case:
This dataset can serve multiple purposes ranging from academic research in biomedical voice signal processing to the practical development of diagnostic tools for early detection of Parkinson's disease. It can be utilized by data scientists and researchers to devise machine learning models capable of distinguishing between healthy individuals and those affected by Parkinson's disease based on voice measurements alone. Furthermore, the dataset can contribute to enhancing our understanding of how Parkinson's disease impacts voice characteristics, aiding in the development of new therapies and treatments.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/46094) of an [OpenML dataset](https://www.openml.org/d/46094). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/46094/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/46094/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/46094/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

